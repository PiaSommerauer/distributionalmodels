This repository contains the distributional models used in my PhD research. 

The models were trained using the Word2vec sgns method with the settings recommended by Levy & Goldberg (2015). 


Training data:


* Gigawords (5th edition)
* Wikipedia 2017 full dump